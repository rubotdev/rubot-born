#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Exit immediately if a command exits with a non-zero status.
# set -e

NODEFILE="node-v16.19.1-linux-armv7l"
#node-v16.18.0-linux-armv6l
#userpass = "170477"

echo "${NODEFILE}.tar.xz"
mkdir /usr/node -m 777
tar -xvf "${NODEFILE}.tar.xz" -C /usr/node
mv "/usr/node/${NODEFILE}"/* /usr/node
rmdir "/usr/node/${NODEFILE}"
rm "${NODEFILE}.tar.xz"

update-alternatives --install "/usr/bin/node" "node" "/usr/node/bin/node" 1
update-alternatives --install "/usr/bin/npm" "npm" "/usr/node/bin/npm" 1
update-alternatives --install "/usr/bin/npx" "npx" "/usr/node/bin/npx" 1


#sudo raspi-config

# Network Options     ->  Hostname 
# raspi-config nonint do_hostname $1

# Change User Password
# (echo \"${userpass}\" ; echo \"${userpass}\" ; echo \"${userpass}\") | passwd

# Interfacing Options ->  Serial  -> No -> Yes
raspi-config nonint do_camera 0
raspi-config nonint do_serial 2

# Localisation Options  ->  Change Wi-fi Country   -> Russia
raspi-config nonint do_wifi_country Russia

# Localisation Options  ->  Change Timezone -> Novosibirsk

# Advanced Options   ->   Expand Filesystem   
# raspi-config nonint do_expand_rootfs

#Restart

# npm install -g nodemon

# update-alternatives --install "/usr/bin/nodemon" "nodemon" "/usr/node/bin/nodemon" 1

# iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 3000

apt update -y
apt full-upgrade -y
apt-get install -y wget git-core cmake
apt-get install -y pigpio
apt-get install -y ffmpeg
apt clean


#crontab -e 
### add string from file crontab

# copy project files
#npm install
