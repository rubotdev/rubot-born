#
#  For the Pi or any computer with less than 2GB of memory
#
if [ -f /etc/dphys-swapfile ]; then
  sudo sed -i 's/^CONF_SWAPSIZE=[0-9]*$/CONF_SWAPSIZE=1024/' /etc/dphys-swapfile
  sudo /etc/init.d/dphys-swapfile stop
  sudo /etc/init.d/dphys-swapfile start
fi

unzip nodesrv.zip

cd ~/nodesrv

npm i

if [ -f /etc/dphys-swapfile ]; then
  sudo sed -i 's/^CONF_SWAPSIZE=[0-9]*$/CONF_SWAPSIZE=0/' /etc/dphys-swapfile
  sudo /etc/init.d/dphys-swapfile stop
  sudo /etc/init.d/dphys-swapfile start
fi

croncmd="nodemon -w /home/pi/nodesrv --config /home/pi/nodesrv/nodemon.json /home/pi/nodesrv/srv.js >> /home/pi/nodesrv/logs/dlog.txt 2>>/home/pi/nodesrv/logs/dlog.err &"
cronjob="@reboot $croncmd"

#To add it to the crontab, with no duplication:
( crontab -l | grep -v -F "$croncmd" ; echo "$cronjob" ) | crontab -

#To remove it from the crontab whatever its current schedule:
#( crontab -l | grep -v -F "$croncmd" ) | crontab -

nodemon -w /home/pi/nodesrv --config /home/pi/nodesrv/nodemon.json /home/pi/nodesrv/srv.js >> /home/pi/nodesrv/logs/dlog.txt 2>>/home/pi/nodesrv/logs/dlog.err &