# Make your own RuBot Raspberry PI robot

## Создай свой РуБот на базе Raspberry PI

## Prerequisites

Flash the last Raspbian Buster Lite image:

<http://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2020-05-28/2020-05-27-raspios-buster-lite-armhf.zip>

or

<https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2022-09-26/2022-09-22-raspios-bullseye-armhf-lite.img.xz>

or

<https://downloads.raspberrypi.org/raspios_lite_armhf_latest>

Open just flashed sd and put your "wpa_supplicant.conf" and an empty "ssh" file inside the boot partition

## Installation

Connect to your Raspberry Pi via SSH

- `wget https://gitlab.com/rubotdev/rubot-born/-/raw/master/prepare.sh`
- `wget https://nodejs.org/dist/v16.19.1/node-v16.19.1-linux-armv7l.tar.xz`
- `sudo bash prepare.sh`
- `sudo nano ~/nodesrv/cfg.json`

Change the "Demo" login and the "Default" password to match your own robot account
sudo reboot

For raspberry pi zero

- `wget https://gitlab.com/rubotdev/rubot-born/-/raw/master/prepare6.sh`
- `wget https://unofficial-builds.nodejs.org/download/release/v16.19.1/node-v16.19.1-linux-armv6l.tar.xz`
- `sudo bash prepare6.sh`
- `sudo nano ~/nodesrv/cfg.json`

Take a look at the default server <https://www.rubot.com>
