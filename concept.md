
# Concepts of bot

## Core

- auth
- robot_id
- log

## Control

- estop ?
- direct control
- mode

## State

- image
- map
- state

## Mission

- robot-mission

Network to connect to the robot

client application can work inside or remote

# About robot

Robot consist of

- Body
- extension port

Robot has main camera and several additional cameras

Coordinate system

bot uses left handed coordinate system
the X axis is directed forward
the Y axis is directed to the right
the Z axis is pointing up

Robot's ZERO point placed at ground level between wheels with 2 wheel configuration

ppositive yaw when 'top' move to the right
positive pitch when 'top' move back (front up)
positive roll when 'front' move to the right

coordinates (position, length and so on) are measured in mm, (mass measured in grams, time in seconds)

## coordinate systems

World coordinate system

by default world coordinate system has (0,0,0) coordinates at point of robot switched on
